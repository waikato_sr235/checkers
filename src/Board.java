import java.awt.Point;

/**
 * An object representing a traditional 8x8 chess board
 * @author Sam Rosenberg
 */
public class Board {
    private Square[][] squares;
    private Square currentChainPosition;
    private PlayingPiece[] playerOnePieces;
    private PlayingPiece[] playerTwoPieces;

    public Board() {
        initialiseSquares();
        setupCheckers(); 
    }

    public PlayingPiece[] getPlayerOnePieces() { return playerOnePieces; }

    public PlayingPiece[] getPlayerTwoPieces() { return playerTwoPieces; }

    /**
     * Initialises squares as a 2d 8x8 array
     */
    private void initialiseSquares() {
        this.squares = new Square[8][8];
        for (int i = 0; i < squares.length; i++) {
            for (int j = 0; j < squares.length; j++) {
                this.squares[i][j] = new Square(i, j);
            }
        }
    }
    
    /**
     * Populates the board with the starting position for checkers
     */
    private void setupCheckers() {
        playerOnePieces = new PlayingPiece[12];
        playerTwoPieces = new PlayingPiece[12];

        int playerId = 1;
        PlayingPiece[] pieces = playerOnePieces;
        int pieceCount = 0;

        for (int i = 0; i < squares.length; i++) {
            if (i > 2 && i < 5) {
                continue; //skip rows 3 and 4 as these have no starting pieces
            }

            for (int j = 0; j < squares.length; j++) {  //even numbered rows get pieces in even numbered columns, odd numbered rows get pieces odd numbered columns
                if (i % 2 == 0) {       
                    if (j % 2 == 0) {   
                        squares[i][j].setPresence(playerId);
                        pieces[pieceCount] = new PlayingPiece(playerId, 0, new Point(i,j));
                        pieceCount++;
                    }
                } else {
                    if (j % 2 != 0) {   
                        squares[i][j].setPresence(playerId);
                        pieces[pieceCount] = new PlayingPiece(playerId, 0, new Point(i,j));
                        pieceCount++;
                    }
                }
            }
            
            if (i == 2) {
                playerId = -1;
                pieces = playerTwoPieces;
                pieceCount = 0;
            }
        }
    }

    /**
     * Activates a space - the piece present is about to either move or take
     * does not validate that a piece is present
     * @param location Point object containing the coords of the selected square
     * @return Square object corresponding to the co-ordinates given
     */
    public Square activateSpace(Point location) {
        return squares[location.x][location.y];
    }

    /**
     * Moves a checker from one square to another; does not validate that the move is legal.
     *
     * @param from square piece is moving from
     * @param to square piece is moving to
     */
    public void move(Square from, Square to) {
        to.setPresence(from.getPresence());
        from.setPresence(0);
    }

    /**
     * The taker will take the selected piece, removing it from the board
     * May result in a null pointer exception if not a legal scenario for taking
     * @param taker square where the piece about the start begins
     * @param taken square where the piece being taken begins
     * @return the current instance of the Board (for additional chain takes)
     */
    public Board take(Square taker, Square taken) {
        taker.setPresence(0);
        taken.setPresence(0);
        locationAfterTaking(taker, taken).setPresence(taker.getPresence());
        return this;
    }

    /**
     * To be used when chaining takes in a checkers game
     * @param taken the subsequent piece being taken
     * @return the current instance of the Board (for additional chain takes)
     */
    public Board take(Square taken) {
        taken.setPresence(0);

        Square nextPosition = locationAfterTaking(currentChainPosition, taken); //variable needed as this needs to be compared with the next space

        nextPosition.setPresence(currentChainPosition.getPresence());
        currentChainPosition.setPresence(0);

        currentChainPosition = nextPosition;
        return this;
    }

    /**
     * Resets the board to it's starting state
     */
    public void reset() {
        for (int i = 0; i < squares.length; i++) {
            for (int j = 0; j < squares.length; j++) {
                squares[i][j].setPresence(0);
            }
        }
        setupCheckers();
    }

    /**
     * Extracted into its own method to allow for multiple different taking methods (checkers, chess)
     * May return null - validation is expected prior to the take method being called
     * @param taker the square where the piece that is moving originates
     * @param taken the square where the piece is being taken from
     * @return the square the taker will end up in; will return null if the square is off the board
     */
    private Square locationAfterTaking(Square taker, Square taken) {
        return offset(taker,taken.getXLocation() - taker.getXLocation() * 2,taken.getYLocation() - taker.getYLocation() * 2);
    }

    /**
     * Returns the square a given offset away from another square
     * Returns null if there is not a valid square at that position
     * a negative offset will move backwards, a positive offset will move forwards
     * @param toOffset the square to move
     * @param xOffset offset of the x coordinate
     * @param yOffset offset of the x coordinate
     * @return square at given offset, or null if illegal offset
     */
    private Square offset(Square toOffset, int xOffset, int yOffset) {
        int newXLocation = toOffset.getXLocation() + xOffset;
        int newYLocation = toOffset.getYLocation() + yOffset;

        if (isValidCoord(newXLocation) && isValidCoord(newYLocation)) {
            return squares[newXLocation][newYLocation];
        } else {
            return null;
        }
    }

    /**
     * checks if a given coordinate is valid (i.e. between 0 and 7)
     * @param coord coordinate to test
     * @return boolean
     */
    private boolean isValidCoord(int coord) {
        return (coord >= 0 && coord < 8);
    }
}
