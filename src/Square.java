import java.awt.Point;

public class Square {
    private Point location;
    private int presence;

    public Square(int xLocation, int yLocation) {
        this.location = new Point(xLocation, yLocation);
        presence = 0;
    }

    public Square(int xLocation, int yLocation, int presence) {
        this.location = new Point(xLocation, yLocation);
        this.presence = presence;
    }

    public int getPresence() {
        return presence;
    }

    public void setPresence(int presence) {
        this.presence = presence;
    }

    public int getXLocation() { return location.x; }
    public int getYLocation() { return location.y; }

}
