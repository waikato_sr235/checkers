import java.awt.Point;

/**
 * A class that represents a playing piece on the board
 */
public class PlayingPiece {
    private int playerId;
    private int pieceType;
    private boolean isTaken;
    private Point location;
    private Point[] movementRules;

    /**
     * @param playerId the owner of the piece; cannot be changed from outside the class
     * @param location where the piece is currently located on the board
     */
    public PlayingPiece(int playerId, int pieceType, Point location) {
        this.playerId = playerId;
        this.location = location;
        this.pieceType = pieceType;
        this.movementRules = MovementRules.forPiece(pieceType, playerId);
        isTaken = false; //just to be explicit
    }

    public int getPlayerId() { return this.playerId; }

    public void setTaken(boolean taken) { isTaken = taken; }
    public boolean isTaken() { return isTaken; }

    public int getPieceType() { return pieceType; }

    public void setPieceType(int pieceType) { this.pieceType = pieceType; }

    /**
     * @return a copy of the Point containing the location of the current piece
     */
    public Point getLocation() { return new Point(location); }

    /**
     * @param location sets this is the new location of the piece
     */
    public void setLocation(Point location) {
        this.location = location;   //TODO or a copy?
    }

    /**
     * Method for obtaining all locations that the piece could theoretically move to.
     * Does not validate that these are legal board positions, or that they are unoccupied.
     * @return an array of points that have been translated from the current location based on
     */
    public Point[] possibleMoves() {
        Point[] theoreticalPositions = new Point[movementRules.length];
        for (int i = 0; i < movementRules.length; i++) {
            Point nextPosition = new Point(location); //create copy to avoid mutating state
            location.translate(movementRules[i].x, movementRules[i].y);
            theoreticalPositions[i] = nextPosition;
        }
        return theoreticalPositions;
    }


}
