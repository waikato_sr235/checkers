public class Game {
    Board board;
    Player playerOne;
    Player playerTwo;

    public static void main(String[] args) {
        Game game = new Game();
        game.start();
    }

    private void start() {
        board = new Board();

        playerOne = createPlayer(1, board.getPlayerOnePieces());
        playerTwo = createPlayer(-1, board.getPlayerTwoPieces());

        while (true) {

            runGame();

            if (playAgain()) {
                board.reset();
            } else {
                //TODO exit message
                return;
            }
        }
    }

    private Player createPlayer(int i, PlayingPiece[] pieces) {
        return new Player(i, playerName(i), pieces);
    }

    private void runGame() {
        while (true) {
            playerOne.takeTurn();
            if (checkWinner()) {
                break;
            }
            playerTwo.takeTurn();
            if (checkWinner()) {
                break;
            }
        }
    }

    private String playerName(int playerId) {
        if (playerId == 1) {
            return "Player One";
        } else {
            return "Player Two";
        }
    }

    private boolean checkWinner() {
        //TODO write method
        return false;
    }

    private boolean playAgain() {
        //TODO write method
        return true;
    }
}
