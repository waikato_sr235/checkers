public class Player {
    PlayingPiece[] pieces;
    String name;
    int playerId;

    public Player(int playerId, String name, PlayingPiece[] pieces) {
        this.playerId = playerId;
        this.name = name;
        this.pieces = pieces;
    }

    public void takeTurn() {
        /*
        select a space
        activate that space
        piece moves or takes (board state is updated)
        update piece state & player state
        if a take; opponent state updated
         */
    }
}
