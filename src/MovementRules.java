import java.awt.Point;

public class MovementRules {
    /**
     * Required for PlayingPiece constructor; used in setup methods
     * Takes advantage of playerId being 1/-1 for pieces that can only move in a single direction
     * @param pieceType type of piece to return movement rule for
     * @param playerId player to whom the piece belongs
     * @return an array of points with possible movements relative to (0,0)
     */
    public static Point[] forPiece(int pieceType, int playerId) {
        Point[] possibleMovements;
        switch (pieceType) {
            case 0: //men in checkers
                possibleMovements = checkerMen(playerId);
                break;
            case 1: //king in checkers
                possibleMovements = checkerKing();
                break;
            default:
                return null;
        }
        return possibleMovements;
    }

    private static Point[] checkerKing() {
        Point[] possibleMovements = new Point[4];
        possibleMovements[0] = new Point(1, 1);
        possibleMovements[1] = new Point(1, -1);
        possibleMovements[2] = new Point(-1,1);
        possibleMovements[3] = new Point(-1,-1);
        return possibleMovements;
    }

    private static Point[] checkerMen(int playerId) {
        return new Point[]{new Point(playerId, 1), new Point(playerId, -1)};
    }
}
