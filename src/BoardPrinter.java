import java.awt.Point;

public class BoardPrinter {
    private Board board;

    /*  A class that abuses the activateSpace class to test the board's state
        At some point I'll need to fix activate space so that this isn't possible, but in the interim it's kind of handy
     */

    public static void main(String[] args) {
        BoardPrinter test = new BoardPrinter();
        test.run();
    }

    private void run() {
        this.board = new Board();
        tempPrint();

        System.out.println();

        this.board.move(this.board.activateSpace(new Point(2, 6)), this.board.activateSpace(new Point(3,7)));
        tempPrint();
    }

    private void tempPrint() {
        String toPrint = "    0 1 2 3 4 5 6 7";
        for (int i = 7; i > -1; i--) {
            toPrint += "\n " + i + ":";
            for (int j = 0; j < 8; j++) {
                String toDisplay;
                switch (board.activateSpace(new Point(i, j)).getPresence()) {
                    case 1:
                        toDisplay = "1";
                        break;
                    case -1:
                        toDisplay = "2";
                        break;
                    default:
                        toDisplay = ".";
                        break;
                }

                toPrint += " " + toDisplay;
            }
        }
        System.out.println(toPrint);
    }
}
